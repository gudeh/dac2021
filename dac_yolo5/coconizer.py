import os
import shutil
import math
from xml_parser import read_xml


def normalizer(inpt):
    normal = [0,0,0,0]
    # print("before->",inpt)
    normal[0] = inpt[0]+((inpt[2] - inpt[0]) / 2)
    normal[0] = normal[0] / 640
    normal[1] = inpt[1]+((inpt[3] - inpt[1]) / 2)
    normal[1] = normal[1] / 360
    normal[2] = (inpt[2] - inpt[0]) / 640
    normal[3] = (inpt[3] - inpt[1]) / 360
    # print("after->",normal)
    return normal


out_path = "coconized/"
if os.path.exists(out_path):
    shutil.rmtree(out_path)
os.mkdir(out_path)

if os.path.exists(out_path + "images/"):
    shutil.rmtree(out_path + "images/")
os.mkdir(out_path + "images/")
if os.path.exists(out_path + "labels/"):
    shutil.rmtree(out_path + "labels/")
os.mkdir(out_path + "labels/")

train_path = "train_data/"
valid_path = "valid_data/"
test_path = "test_data/"
data_set_path = "../../data_training/"

if os.path.exists(out_path + "images/" + train_path):
    shutil.rmtree(out_path + "images/" + train_path)
if os.path.exists(out_path + "images/" + valid_path):
    shutil.rmtree(out_path + "images/" + valid_path)
if os.path.exists(out_path + "images/" + test_path):
    shutil.rmtree(out_path + "images/" + test_path)
os.mkdir(out_path + "images/" + train_path)
os.mkdir(out_path + "images/" + valid_path)
os.mkdir(out_path + "images/" + test_path)

if os.path.exists(out_path + "labels/" + train_path):
    shutil.rmtree(out_path + "labels/" + train_path)
if os.path.exists(out_path + "labels/" + valid_path):
    shutil.rmtree(out_path + "labels/" + valid_path)
if os.path.exists(out_path + "labels/" + test_path):
    shutil.rmtree(out_path + "labels/" + test_path)
os.mkdir(out_path + "labels/" + train_path)
os.mkdir(out_path + "labels/" + valid_path)
os.mkdir(out_path + "labels/" + test_path)

all_dirs = sorted(os.listdir(data_set_path))
train_file_names = open(out_path + "train.txt", "w")
valid_file_names = open(out_path + "valid.txt", "w")
test_file_names = open(out_path + "test.txt", "w")
# for folder in all_dirs[:10]:
for folder in all_dirs:
    train = []
    valid = []
    test = []
    all_jpgs = sorted([name for name in os.listdir(data_set_path + folder) if name.find(".jpg") != -1])
    folder.replace("/", "")
    for file in all_jpgs[:math.ceil(len(all_jpgs) * 0.8)-1]:  # math.ceil -> round up (ceiling)
        out_file_name = folder + "_" + file
        train.append(file)
        shutil.copyfile(data_set_path + folder + '/' + file,
                        out_path + "images/" + train_path + out_file_name)
        ret = read_xml(data_set_path + folder + '/' + file.replace("jpg", "xml"))
        normalized_ret = normalizer(ret)
        label_file = open(out_path + "labels/" + train_path + out_file_name.replace("jpg", "txt"), "w")
        label_file.write(
            str(all_dirs.index(folder)) + " " + str(normalized_ret[0]) + " " + str(normalized_ret[1]) + " " + str(normalized_ret[2]) + " " + str(normalized_ret[3]) + "\n")
        # print(ret)
        train_file_names.write("./images/" + train_path + out_file_name + "\n")
##################################################################################################################################      
    for file in all_jpgs[math.ceil(len(all_jpgs) * 0.8)-1:-(math.ceil(len(all_jpgs) * 0.1))]:  # int -> round down (floor)
        out_file_name = folder + "_" + file
        valid.append(file)
        shutil.copyfile(data_set_path + folder + '/' + file,
                        out_path + "images/" + valid_path + out_file_name)
        ret = read_xml(data_set_path + folder + '/' + file.replace("jpg", "xml"))
        normalized_ret = normalizer(ret)
        label_file = open(out_path + "labels/" + valid_path + out_file_name.replace("jpg", "txt"), "w")
        label_file.write(
            str(all_dirs.index(folder)) + " " + str(normalized_ret[0]) + " " + str(normalized_ret[1]) + " " + str(normalized_ret[2]) + " " + str(normalized_ret[3]) + "\n")

        valid_file_names.write("./images/" + valid_path + out_file_name + "\n")
##################################################################################################################################        
    for file in all_jpgs[-(math.ceil(len(all_jpgs) * 0.1)):]:  # int -> round down (floor)
        out_file_name = folder + "_" + file
        test.append(file)
        shutil.copyfile(data_set_path + folder + '/' + file,
                        out_path + "images/" + test_path + out_file_name)
        ret = read_xml(data_set_path + folder + '/' + file.replace("jpg", "xml"))
        normalized_ret = normalizer(ret)
        label_file = open(out_path + "labels/" + test_path + out_file_name.replace("jpg", "txt"), "w")
        label_file.write(
            str(all_dirs.index(folder)) + " " + str(normalized_ret[0]) + " " + str(normalized_ret[1]) + " " + str(normalized_ret[2]) + " " + str(normalized_ret[3]) + "\n")

        test_file_names.write("./images/" + test_path + out_file_name + "\n")
    print(folder, "done")
    # print("->", folder, "num images:", len(all_jpgs), "trainSize:", len(train), "testSize:", len(test))

print(all_dirs)

# names=all_dirs
# print(names)
# print(len(names))
