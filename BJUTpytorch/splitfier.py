import os
import shutil
import math
from xml_parser import read_xml

train_path = "../../train_data/"
test_path = "../../coconized/test_data/"
input_dataset_path = "data_training/"
if os.path.exists(train_path):
    shutil.rmtree(train_path)
if os.path.exists(test_path):
    shutil.rmtree(test_path)
os.mkdir(train_path)
os.mkdir(test_path)

all_dirs = sorted(os.listdir(input_dataset_path))
# all_dirs = all_dirs[:1] #testing with first fodler only
for folder in all_dirs:
    train = []
    test = []
    all_jpgs = sorted([name for name in os.listdir(input_dataset_path + folder) if name.find(".jpg") != -1])
    if not os.path.exists(train_path+folder):
        os.mkdir(train_path+folder)
    if not os.path.exists(test_path + folder):
        os.mkdir(test_path + folder)
    for file in all_jpgs[:math.ceil(len(all_jpgs) * 0.8)]:  # math.ceil -> round up (ceiling)
        train.append(file)
        shutil.copyfile(input_dataset_path + folder + '/' + file, train_path + folder + '/' + file)
        shutil.copyfile(input_dataset_path + folder + '/' + file.replace("jpg", "xml"), train_path + folder + '/' + file.replace("jpg", "xml"))
    for file in all_jpgs[-int(len(all_jpgs) * 0.2):]:  # int -> round down (floor)
        test.append(file)
        shutil.copyfile(input_dataset_path + folder + '/' + file, test_path + folder + '/' + file)
        shutil.copyfile(input_dataset_path + folder + '/' + file.replace("jpg", "xml"), test_path + folder + '/' + file.replace("jpg", "xml"))
    print("->", folder, "num images:",len(all_jpgs), "trainSize:",len(train), "testSize:",len(test))

