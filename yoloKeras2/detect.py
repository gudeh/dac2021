import os
import time
import csv
import xml_parser
from absl import app, flags, logging
from absl.flags import FLAGS
import cv2
import numpy as np
import tensorflow as tf
from yolov3_tf2.models import (
    YoloV3, YoloV3Tiny
)
from yolov3_tf2.dataset import transform_images, load_tfrecord_dataset
from yolov3_tf2.utils import draw_outputs, draw_label_only, broadcast_iou

flags.DEFINE_string('classes', './data/coco.names', 'path to classes file')
flags.DEFINE_string('weights', './checkpoints/yolov3.tf',
                    'path to weights file')
flags.DEFINE_boolean('tiny', False, 'yolov3 or yolov3-tiny')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('image', './data/girl.png', 'path to input image')
flags.DEFINE_string('tfrecord', None, 'tfrecord instead of image')
flags.DEFINE_string('output', './output.jpg', 'path to output image')
flags.DEFINE_integer('num_classes', 80, 'number of classes in the model')

output_folder_name = "out_figures_with_BB"
def main(_argv):
    t1=time.time()
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    for physical_device in physical_devices:
        tf.config.experimental.set_memory_growth(physical_device, True)

    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=FLAGS.num_classes)
    else:
        yolo = YoloV3(classes=FLAGS.num_classes)

    # yolo.load_weights(FLAGS.weights).expect_partial()
    logging.info('weights loaded')

    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]
    logging.info('classes loaded')
    base_path = "../data_training/"
    outer_csv = output_folder_name + '/all_folders.csv'
    with open(outer_csv, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["folder name","image count","mean IOU","FPS"])
    folder_info={} # name,mean_iou
    for class_path in os.listdir(base_path):
        inner_csv = output_folder_name + '/' + class_path + 'all_info.csv'
        class_path += '/'
        # if not os.path.exists(inner_csv):
        with open(inner_csv, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(
                ["class", "image name", "threshold score", "target label", "achieved label", "achieved score",
                 "target score", "time to process", "number of images detected", "IOU"])
        os_list_it = iter(os.listdir(base_path + class_path))
        print("folder:",class_path)
        out_folder_path=output_folder_name+'/'+class_path
        if not os.path.exists(out_folder_path):
            os.mkdir(out_folder_path)
        all_iou=[]
        for file in os_list_it:
            name, extension = os.path.splitext(file)
            if extension == ".jpg":
                t1 = time.time()
                image_name = name + ".jpg"
            elif extension == ".xml":
                image_xml = name + ".xml"
                t2 = time.time()
                print("time to handle image file name", t2 - t1)
                # print(class_path,"IMAGE NAME:", image_name)
                
                # if FLAGS.tfrecord:
                #     dataset = load_tfrecord_dataset(
                #         FLAGS.tfrecord, FLAGS.classes, FLAGS.size)
                #     dataset = dataset.shuffle(512)
                #     img_raw, _label = next(iter(dataset.take(1)))
                # else:
                img_raw = tf.image.decode_image(
                    open(FLAGS.image, 'rb').read(), channels=3)
                    # open(base_path + class_path + image_name, 'rb').read(), channels=3)
                img = tf.expand_dims(img_raw, 0)
                img = transform_images(img, FLAGS.size)

                t3 = time.time()
                print("time to decode image", t3 - t2)
                boxes, scores, classes, nums = yolo(img)
                t4 = time.time()
                print("time to get boxes (yolo(img)", t4 - t3)

                label = xml_parser.read_xml(base_path + class_path + image_xml)
                box_label = tf.convert_to_tensor(label)
                t5 = time.time()
                print("time to process label", t5 - t4)

                # with open(inner_csv, 'a', newline='') as file:
                #     writer = csv.writer(file)
                #     t11=time.time()
                #     writer.writerow(
                #         [class_path, image_name, class_threshold, v_labels[-1], v_labels[max_index],
                #          v_scores[-1], v_scores[max_index], t11-t1, len(v_boxes) - 2, iou])
                logging.info('detections:')
                list_iou = []
                for i in range(nums[0]):
                    logging.info('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
                                                       np.array(scores[0][i]),
                                                       np.array(boxes[0][i])))
                    list_iou.append(broadcast_iou(boxes[0][i], box_label))
                t6 = time.time()
                print("time to calculate IOUs", t6 - t5)
                if len(list_iou) > 0:
                    max_iou = tf.math.reduce_max(tf.convert_to_tensor(list_iou))
                else:
                    max_iou = 0
                # print("max iou", np.array(max_iou))
                # print("all iou", all_iou)
                # all_iou=tf.stack([all_iou,max_iou],0)
                all_iou.append(max_iou)
                # print("ALL IOU:",all_iou)
                t7 = time.time()
                print("time to find max IOU", t6 - t7)

                img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)
                img = draw_label_only(img, box_label, class_path)
                img = draw_outputs(img, (boxes, scores, classes, nums), class_names)


                cv2.imwrite(out_folder_path + image_name, img)
                # logging.info('output saved to: {}'.format(out_folder_path+image_name))

                tfinal = time.time()
                print("time to write image", tfinal - t7)
                print("whole time:", tfinal - t1)
        # print(np.array(all_iou))
        # print("all_iou sum",sum(np.array(all_iou)),"all_iou len",len(np.array(all_iou)))
        mean_iou=tf.math.reduce_mean(tf.convert_to_tensor(all_iou))#sum(all_iou)/len(all_iou)
        images_count=len([name for name in os.listdir(base_path+class_path) if name.find(".jpg")!=-1])
        tfolder2=time.time()
        # print(class_path, folder_info[class_path])
        with open(outer_csv, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([class_path, images_count, np.array(mean_iou), (images_count/(tfolder2-tfolder1))])


if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
