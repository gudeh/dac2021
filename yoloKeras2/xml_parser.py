import xml.dom.minidom


# import vamoa


def save_my_results(result_rectangle):
    doc = xml.dom.minidom.Document()
    root = doc.createElement('header')

    # for i in range(len(self.img_list)):
    image_e = root.appendChild(doc.createElement("annotation"))

    # doc.appendChild(root)
    doc.appendChild(image_e)
    name_e = doc.createElement('filename')
    name_e2 = doc.createElement('name')
    # name_t = doc.createTextNode(self.img_list[i].name)
    name_t = doc.createTextNode('TODO_filename')
    name_e.appendChild(name_t)
    image_e.appendChild(name_e)

    size_e = doc.createElement('size')
    node_width = doc.createElement('width')
    node_width.appendChild(doc.createTextNode("640"))
    node_length = doc.createElement('length')
    node_length.appendChild(doc.createTextNode("360"))
    size_e.appendChild(node_width)
    size_e.appendChild(node_length)
    image_e.appendChild(size_e)

    object_node = doc.createElement('object')
    name_t2 = doc.createTextNode('TODO_objectname')
    name_e2.appendChild(name_t2)
    object_node.appendChild(name_e2)
    node_bnd_box = doc.createElement('bndbox')
    node_bnd_box_xmax = doc.createElement('xmax')
    node_bnd_box_xmax.appendChild(doc.createTextNode(str(result_rectangle[0].xmax)))  # [i][1])))
    node_bnd_box_xmin = doc.createElement('xmin')
    node_bnd_box_xmin.appendChild(doc.createTextNode(str(result_rectangle[0].xmin)))  # [i][0])))
    node_bnd_box_ymax = doc.createElement('ymax')
    node_bnd_box_ymax.appendChild(doc.createTextNode(str(result_rectangle[0].ymax)))  # [i][3])))
    node_bnd_box_ymin = doc.createElement('ymin')
    node_bnd_box_ymin.appendChild(doc.createTextNode(str(result_rectangle[0].ymin)))  # [i][2])))
    node_bnd_box.appendChild(node_bnd_box_xmax)
    node_bnd_box.appendChild(node_bnd_box_xmin)
    node_bnd_box.appendChild(node_bnd_box_ymax)
    node_bnd_box.appendChild(node_bnd_box_ymin)

    object_node.appendChild(node_bnd_box)
    image_e.appendChild(object_node)

    with open("outs.xml", 'w') as fp:
        doc.writexml(fp, indent='\t', addindent='\t', newl='\n', encoding="utf-8")


def read_xml(filename):
    doc = xml.dom.minidom.parse(filename)
    # print("------------reading xml:",doc.nodeName, "first child:",doc.firstChild.tagName)
    xmin = int(doc.getElementsByTagName("xmin")[0].firstChild.nodeValue)
    xmax = int(doc.getElementsByTagName("xmax")[0].firstChild.nodeValue)
    ymin = int(doc.getElementsByTagName("ymin")[0].firstChild.nodeValue)
    ymax = int(doc.getElementsByTagName("ymax")[0].firstChild.nodeValue)
    # print("xmax:",xmax,"xmin:",xmin,"ymax:",ymax,"ymin:",ymin)
    # return([xmax,xmin,ymax,ymin])
    return ([xmin/640, ymin/360, xmax/640, ymax/360])
# print(xmax.getAttribute("xmax"))
# for node in xmax:
# print("node:",xmax[0].firstChild.nodeValue,"type:",type(node))
