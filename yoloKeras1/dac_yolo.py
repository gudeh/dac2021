# load yolov3 model and perform object detection
# based on https://github.com/experiencor/keras-yolo3
import csv
import numpy as np
import tensorflow as tf
import os
from os import path
from numpy import expand_dims
from keras.models import load_model
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from matplotlib import pyplot
from matplotlib.patches import Rectangle


import time
#from timeit import default_timer as timer

# import dac_sdc
import xml_parser

from tensorflow.python.client import device_lib

print(device_lib.list_local_devices())

# import tensorflow as tf
# sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

# define the labels
labels = ["person", "bicycle", "car", "motorbike", "aeroplane", "bus", "train", "truck",
          "boat", "traffic light", "fire hydrant", "stop sign", "parking meter", "bench",
          "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe",
          "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee", "skis", "snowboard",
          "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard",
          "tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl", "banana",
          "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza", "donut", "cake",
          "chair", "sofa", "pottedplant", "bed", "diningtable", "toilet", "tvmonitor", "laptop",
          "mouse", "remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink", "refrigerator",
          "book", "clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush"]
# labels=["boat","person"]
# labels = ["person", "bicycle", "car", "motorbike", "aeroplane", "bus", "train", "truck","boat"]
base_output_folder = "out_figures_with_BB"

anchors = [[116, 90, 156, 198, 373, 326], [30, 61, 62, 45, 59, 119], [10, 13, 16, 30, 33, 23]]
class_threshold = 0.55

class BoundBox:
    def __init__(self, xmin, ymin, xmax, ymax, objness=None, classes=None):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.objness = objness
        self.classes = classes
        self.label = -1
        self.score = -1

    def get_label(self):
        if self.label == -1:
            self.label = np.argmax(self.classes)

        return self.label

    def get_score(self):
        if self.score == -1:
            self.score = self.classes[self.get_label()]

        return self.score


def _sigmoid(x):
    return 1. / (1. + np.exp(-x))


def decode_netout(netout, anchors, obj_thresh, net_h, net_w):
    grid_h, grid_w = netout.shape[:2]
    nb_box = 3
    netout = netout.reshape((grid_h, grid_w, nb_box, -1))
    nb_class = netout.shape[-1] - 5
    boxes = []
    # dummy=[]
    # boxes=np.array(dummy,dtype=BoundBox)
    netout[..., :2] = _sigmoid(netout[..., :2])
    netout[..., 4:] = _sigmoid(netout[..., 4:])
    netout[..., 5:] = netout[..., 4][..., np.newaxis] * netout[..., 5:]
    netout[..., 5:] *= netout[..., 5:] > obj_thresh

    for i in range(grid_h * grid_w):
        row = i / grid_w
        col = i % grid_w
        for b in range(nb_box):
            # 4th element is objectness score
            objectness = netout[int(row)][int(col)][b][4]
            # if (objectness.all() <= obj_thresh): continue
            if (objectness <= obj_thresh).all(): continue
            # first 4 elements are x, y, w, and h
            x, y, w, h = netout[int(row)][int(col)][b][:4]
            x = (col + x) / grid_w  # center position, unit: image width
            y = (row + y) / grid_h  # center position, unit: image height
            w = anchors[2 * b + 0] * np.exp(w) / net_w  # unit: image width
            h = anchors[2 * b + 1] * np.exp(h) / net_h  # unit: image height
            # last elements are class probabilities
            classes = netout[int(row)][col][b][5:]
            box = BoundBox(x - w / 2, y - h / 2, x + w / 2, y + h / 2, objectness, classes)
            boxes.append(box)
            # boxes=np.append(boxes,box)
    return boxes


def correct_yolo_boxes(boxes, image_h, image_w, net_h, net_w):
    new_w, new_h = net_w, net_h
    for i in range(len(boxes)):
        x_offset, x_scale = (net_w - new_w) / 2. / net_w, float(new_w) / net_w
        y_offset, y_scale = (net_h - new_h) / 2. / net_h, float(new_h) / net_h
        boxes[i].xmin = int((boxes[i].xmin - x_offset) / x_scale * image_w)
        boxes[i].xmax = int((boxes[i].xmax - x_offset) / x_scale * image_w)
        boxes[i].ymin = int((boxes[i].ymin - y_offset) / y_scale * image_h)
        boxes[i].ymax = int((boxes[i].ymax - y_offset) / y_scale * image_h)


def _interval_overlap(interval_a, interval_b):
    # print(type(interval_a),type(interval_b))
    x1, x2 = interval_a
    x3, x4 = interval_b
    # print(type(x1),type(x2),type(x3),type(x4))
    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2, x4) - x1
    else:
        if x2 < x3:
            return 0
        else:
            return min(x2, x4) - x3


def bbox_iou(box1, box2):
    # print(box1,box2)
    # print(type(box1),type(box2))
    intersect_w = _interval_overlap([box1.xmin, box1.xmax], [box2.xmin, box2.xmax])
    intersect_h = _interval_overlap([box1.ymin, box1.ymax], [box2.ymin, box2.ymax])
    intersect = intersect_w * intersect_h
    w1, h1 = box1.xmax - box1.xmin, box1.ymax - box1.ymin
    w2, h2 = box2.xmax - box2.xmin, box2.ymax - box2.ymin
    union = w1 * h1 + w2 * h2 - intersect
    return float(intersect) / union


def do_nms(boxes, nms_thresh):
    if len(boxes) > 0:
        nb_class = len(boxes[0].classes)
    else:
        return
    for c in range(nb_class):
        sorted_indices = np.argsort([-box.classes[c] for box in boxes])
        for i in range(len(sorted_indices)):
            index_i = sorted_indices[i]
            if boxes[index_i].classes[c] == 0: continue
            for j in range(i + 1, len(sorted_indices)):
                index_j = sorted_indices[j]
                if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_thresh:
                    boxes[index_j].classes[c] = 0


# load and prepare an image
def load_image_pixels(filename, shape):
    # load the image to get its shape
    image = load_img(filename)
    width, height = image.size
    # load the image with the required size
    image = load_img(filename, target_size=shape)
    # convert to numpy array
    image = img_to_array(image)
    # scale pixel values to [0, 1]
    image = image.astype('float32')
    image /= 255.0
    # add a dimension so that we have one sample
    image = expand_dims(image, 0)
    return image, width, height


# get all of the results above a threshold
def get_boxes(boxes, labels, thresh):
    v_boxes, v_labels, v_scores = list(), list(), list()
    # enumerate all boxes
    for box in boxes:
        # enumerate all possible labels
        for i in range(len(labels)):
            # check if the threshold for this label is high enough
            if box.classes[i] > thresh:
                v_boxes.append(box)
                v_labels.append(labels[i])
                v_scores.append(box.classes[i] * 100)
            # don't break, many labels may trigger for one box
    return v_boxes, v_labels, v_scores


# draw all results
def draw_boxes(filename, class_path, base_path, v_boxes, v_labels, v_scores):
    # load the image
    data = pyplot.imread(base_path + class_path + filename)
    # plot the image
    pyplot.imshow(data)
    # get the context for drawing boxes
    ax = pyplot.gca()
    # plot each box
    for i in range(len(v_boxes)):
        box = v_boxes[i]
        # get coordinates
        y1, x1, y2, x2 = box.ymin, box.xmin, box.ymax, box.xmax
        # calculate width and height of the box
        width, height = x2 - x1, y2 - y1
        # create the shape
        rect = Rectangle((x1, y1), width, height, fill=False, color='C' + str(i))  # , color='white')
        # draw the box
        ax.add_patch(rect)
        # draw text and score in top left corner
        label = "%s (%.3f)" % (v_labels[i], v_scores[i])
        pyplot.text(x1, y1, label, color='white')
    # show the plot
    # pyplot.show()
    out_class_path = './' + base_output_folder + '/' + class_path
    if not os.path.exists(out_class_path):
        os.mkdir(out_class_path)
    pyplot.savefig(out_class_path + '/' + filename)
    pyplot.close()


if not os.path.exists(base_output_folder):
	os.mkdir(base_output_folder)
# load yolov3 model
model = load_model('model.h5', compile=False)
# define the expected input shape for the model
input_w, input_h = 416, 416

base_path = "../../data_training/"
outer_csv = base_output_folder + '/all_folders.csv'
with open(outer_csv, 'a', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["folder name","image count","mean IOU","FPS"])
for class_path in sorted(os.listdir(base_path)):
    tfolder1=time.time()
    inner_csv = base_output_folder + '/' + class_path + 'all_info.csv'
    class_path += '/'
    # if not os.path.exists(inner_csv):
    with open(inner_csv, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["class", "image name", "threshold score", "target label", "achieved label", "achieved score",
                         "target score","time to process", "number of images detected", "IOU"])
    os_list_it = iter(sorted(os.listdir(base_path + class_path)))
    all_iou = []
    for file in os_list_it:
        name, extension = os.path.splitext(file)
        print(class_path)
        print(name,extension)
        # if os.path.exists('./' + base_output_folder + '/' + class_path + '/' + name + '.jpg'):
        #     print("Skipping:", class_path, name)
        # else:
        if extension == ".jpg":
            image_name = name + ".jpg"
        elif extension == ".xml":
            image_xml = name + ".xml"
            print("IMAGE NAME:", image_name, "IMAGE XML:", image_xml)
            t1=time.time()
            label = xml_parser.read_xml(base_path + class_path + image_xml)
            label = [int(i) for i in label]
            bbox_label = BoundBox(label[0], label[1], label[2], label[3])
            # load and prepare image
            image, image_w, image_h = load_image_pixels(base_path + class_path + image_name, (input_w, input_h))
            # make prediction
            t3=time.time()
            yhat = model.predict(image)
            t4=time.time()
            print("time to predict", (t4 - t3))
            # summarize the shape of the list of arrays
            # print([a.shape for a in yhat])
            boxes = []
            # dummy = []
            # boxes = np.array(dummy, dtype=BoundBox)
            # boxes *= 0
            for i in range(len(yhat)):
                # decode the output of the network
                boxes += decode_netout(yhat[i][0], anchors[i], class_threshold, input_h, input_w)
                # boxes=np.append(boxes,decode_netout(yhat[i][0], anchors[i], class_threshold, input_h, input_w))
            t5=time.time()
            print("time to set boxes (decode_netout)", t5 - t4)
            # correct the sizes of the bounding boxes for the shape of the image
            correct_yolo_boxes(boxes, image_h, image_w, input_h, input_w)
            t6=time.time()
            print("time to correct boxes", t6 - t5)
            # suppress non-maximal boxes
            # do_nms(boxes, 0.5)
            # t7=time.time()
            # print("time to suppres non-maximal boxes", t7 - t6)

            # get the details of the detected objects
            v_boxes, v_labels, v_scores = get_boxes(boxes, labels, class_threshold)
            t8=time.time()
            print("time to get_boxes objects (run labels list)", t8 - t6)

            # empty box, score 0 when no answer found
            dummy_box = BoundBox(0, 0, 0, 0)
            v_boxes.append(dummy_box)
            v_labels.append('empty')
            v_scores.append(0)
            max_index = v_scores.index(max(v_scores, default=0))
            iou = bbox_iou(v_boxes[max_index], bbox_label)
            all_iou.append(iou)
            # print(class_path, image_name, v_labels[-1], v_scores[-1], v_labels[0],v_scores[0],iou,len(v_boxes)-1, class_threshold)
            # print("ymin", v_boxes[0].ymin, "xmin", v_boxes[0].xmin, "ymax", v_boxes[0].ymax, "xmax", v_boxes[0].xmax)
            # print("ymin", v_boxes[1].ymin, "xmin", v_boxes[1].xmin, "ymax", v_boxes[1].ymax, "xmax", v_boxes[1].xmax)

            # draw and show image png
            v_boxes.append(bbox_label)
            v_labels.append(class_path)
            v_scores.append(100)
            # print("v_boxes size",len(v_boxes))
            with open(inner_csv, 'a', newline='') as file:
                writer = csv.writer(file)
                t11=time.time()
                writer.writerow(
                    [class_path, image_name, class_threshold, v_labels[-1], v_labels[max_index],
                     v_scores[-1], v_scores[max_index], t11-t1, len(v_boxes) - 2, iou])
            print("time for everything", t11-t1, "\n")
            # all_files_info.append()
            # print(all_files_info)
            # print("v_boxes:", v_boxes, "v_labels:", v_labels, "v_scores:", v_scores)
            # if not os.path.exists('./' + base_output_folder + '/' + class_path + '/' + name + '.jpg'):
            draw_boxes(image_name, class_path, base_path, v_boxes, v_labels, v_scores)
    mean_iou=tf.math.reduce_mean(tf.convert_to_tensor(all_iou))#sum(all_iou)/len(all_iou)
    images_count=len([name for name in os.listdir(base_path+class_path) if name.find(".jpg")!=-1])
    tfolder2=time.time()
    # print(class_path, folder_info[class_path])
    with open(outer_csv, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([class_path, images_count, np.array(mean_iou), (images_count/(tfolder2-tfolder1))])

